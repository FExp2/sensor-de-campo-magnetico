# Sensor de Campo Magnético

<img src="img/fotoSensor.jpg" alt="fig. 1" width="300"/>

<img src="img/ky003.jpg" alt="fig. 1" width="300"/>

## Descripción

Este es un sensor de efecto Hall para medir intensidad de campo magnético.

El [efecto Hall](https://en.wikipedia.org/wiki/Hall_effect) consiste en una diferencia de potencial que se produce sobre un conductor
eléctrico por el que circula una corriente, cuando el mismo se encuentra inmerso en un campo magnético.

El módulo ky-024, incluye un [sensor de efecto hall](https://sensing.honeywell.com/SS49E-linear-and-angle-sensor-ics) y un 
[amplificador comparador](http://www.ti.com/lit/ds/symlink/lm393.pdf) que permite utilizar el módulo como interruptor magnético.

El módulo ky-003 sólo incluye el [sensor de efecto hall](https://sensing.honeywell.com/SS49E-linear-and-angle-sensor-ics)
## Especificaciones

- Tensión de operación: 2.7V a 6.5V
- Sensibilidad: 1.00mV/G a 1.75mV/G 

El módulo tiene 4 terminales (pines) de conexión:
- A0: salida analógica
- G: conectar a GND (ground)
- +: conectar a Vcc (+5V)
- D0: salida digital tipo switch configurable mediante el potenciómetro

## Diagrama de conexión con Arduino

<img src="img/Schematic.png" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software)

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.
8. Abrir el *monitor serial* o el *serial plotter*

<img src="img/arduinoIDE_4.png" alt="fig. 1" width="300"/>

9. Asegurarse que la velocidad de comunicación está seteada en 9600 bauds

### AHORA ACERCÁ Y ALEJÁ UN IMÁN AL SENSOR!!!
